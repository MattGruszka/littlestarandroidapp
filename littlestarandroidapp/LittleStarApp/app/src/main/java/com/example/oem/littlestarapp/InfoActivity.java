package com.example.oem.littlestarapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InfoActivity extends AppCompatActivity {

    @BindView(R.id.buttonClose)
    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        hideStatusBar();
        ButterKnife.bind(this);
    }

    public void hideStatusBar(){
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @OnClick(R.id.buttonClose)
    public void close() {
        Intent i2 = new Intent(this, MainActivity.class);
        startActivity(i2);
        overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
    }
}
