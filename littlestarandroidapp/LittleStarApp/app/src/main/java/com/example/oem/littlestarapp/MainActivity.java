package com.example.oem.littlestarapp;

import android.app.ActionBar;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

import static java.lang.StrictMath.abs;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ButtonMain)
    Button putMeBtn;
    @BindView(R.id.buttonStartOver)
    Button startOverBtn;
    @BindView(R.id.buttonInfo)
    Button infoBtn;
    @BindView(R.id.targetValue)
    TextView targetValueText;
    @BindView(R.id.numberScore)
    TextView numberScore;
    @BindView(R.id.numberRound)
    TextView numberRound;
    @BindView(R.id.seekBar2)
    SeekBar seekBar;

    private int currentValue = 50;
    private int targetValue = 0;
    private int score = 0;
    private int round = 0;
    private int totalScore = 0;
    private String reaction = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hideStatusBar();
        ButterKnife.bind(this);

        currentValue = seekBar.getProgress();
        seekBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentValue = seekBar.getProgress();
            }
        });

        play();
    }

    private void play() {
        startNewRound();
    }

    private void startNewRound(){
        seekBar.setProgress(50);
        round++;
        Random r = new Random();
        targetValue = r.nextInt(100)+1;
        updateLabels();
    }
    public void calculate() {
        score = 100 - abs(targetValue - currentValue);
        if (score==100) {
            reaction = "PERFECT! DOUBLE POINTS!";
        } else if (score < 100 && score >= 90) {
            reaction = "That was really CLOSE!";
        } else if (score < 90 && score >= 80) {
            reaction = "Well done!";
        } else if (score < 80 && score >= 70) {
            reaction = "Nice!";
        } else if (score < 70 && score >= 60) {
            reaction = "You can do better!";
        } else if (score < 60 && score >= 50) {
            reaction = "Try again!";
        } else if (score < 50) {
            reaction = "Not even close...";
        }
        totalScore += score;
    }

    private void updateLabels(){
        numberRound.setText(Integer.toString(round));
        targetValueText.setText(Integer.toString(targetValue));
        numberScore.setText(Integer.toString(totalScore));

    }

    @OnClick(R.id.ButtonMain)
    public void putMe() {
        currentValue = seekBar.getProgress();
        calculate();
        hideStatusBar();
        new AlertDialog.Builder(this)
                .setTitle(reaction)
                .setMessage("You scored " + String.valueOf(score) + " points.")
                .setPositiveButton("awesome", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        hideStatusBar();
                        startNewRound();
                        updateLabels();
                    }
                })

                .setIcon(android.R.drawable.ic_popup_reminder)
                .show();
    }


    @Override
    public void onResume() {
        super.onResume();
        hideStatusBar();
    }


    public void hideStatusBar(){
        final View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                            decorView.setSystemUiVisibility(uiOptions);
                        } else {
                            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                            decorView.setSystemUiVisibility(uiOptions);
                        }
                    }
                });
    }

    @OnClick(R.id.buttonStartOver)
    public void startOver() {
        totalScore = 0;
        score = 0;
        round = 0;
        startNewRound();
        updateLabels();
    }

    @OnClick(R.id.buttonInfo)
    public void info() {
        Intent i = new Intent(this, InfoActivity.class);
        startActivity(i);
        overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
    }
}
